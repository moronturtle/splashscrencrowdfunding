import React, {useEffect} from 'react';
import {Alert} from 'react-native';
import AppNavigation from './src/routes';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';
import {TabRouter} from '@react-navigation/native';

let firebaseConfig = {
  apiKey: 'AIzaSyAxdktvnz6WWNko6rtuqpaCO_s1iE_rwHE',
  authDomain: 'sanbercode-87dd6.firebaseapp.com',
  projectId: 'sanbercode-87dd6',
  storageBucket: 'sanbercode-87dd6.appspot.com',
  messagingSenderId: '642732183614',
  appId: '1:642732183614:web:38219ed80034b0917fc16f',
  measurementId: 'G-XYDVJ3CD79',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  // useEffect(() => {
  //   OneSignal.setLogLevel(6, 0);

  //   OneSignal.init('19cc54ed-591b-48e5-8929-b1ded8a68bc0', {
  //     kOSSettingsKeyAutoPrompt: false,
  //     kOSSettingsKeyInAppLaunchURL: false,
  //     kOSSettingsKeyInFocusDisplayOption: 2,
  //   });
  //   OneSignal.inFocusDisplaying(2);

  //   OneSignal.addEventListener('received', onReceived);
  //   OneSignal.addEventListener('opened', onOpened);
  //   OneSignal.addEventListener('ids', onIds);

  //   codePush.sync(
  //     {
  //       updateDialog: true,
  //       installMode: codePush.InstallMode.IMMEDIATE,
  //     },
  //     SyncStatus,
  //   );

  //   return () => {
  //     OneSignal.removeEventListener('received');
  //     OneSignal.removeEventListener('opened');
  //     OneSignal.removeEventListener('ids');
  //   };
  // }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for Update');
        break;

      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('DownloadiNG Package');
        break;

      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to Date');
        break;

      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing Update');
        break;

      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update Installed');
        break;

      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting User', 'Update Installed');
        break;

      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('onReceived', notification);
  };

  const onOpened = (openResult) => {
    console.log('onReceived', openResult);
  };

  const onIds = (device) => {
    console.log('onIds', device);
  };

  return <AppNavigation />;
};

export default App;
