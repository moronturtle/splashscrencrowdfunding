import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './screens/Login';
import EditAccount from './screens/Profile/EditAccount';
import Bantuan from './screens/Bantuan';
import Statistics from './screens/Statistik';
import History from './screens/History';
import Donasi from './screens/Donasi';
import DetailDonasi from './screens/Donasi/Detail';
import CreateDonasi from './screens/CreateDonasi';
import Payment from './screens/Payment';
import Intro from './screens/Intro';
import Register from './screens/Register';
import Otp from './screens/Register/Otp';
import RegisterPassword from './screens/Register/InputPassword';
import HomeTabs from './navigation/HomeTabs';

const Stack = createStackNavigator();
const RegisterStack = createStackNavigator();

const RegisterNavigation = () => {
  return (
    <RegisterStack.Navigator>
      <RegisterStack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <RegisterStack.Screen
        name="Otp"
        component={Otp}
        options={{headerShown: false}}
      />
      <RegisterStack.Screen
        name="RegisterPassword"
        component={RegisterPassword}
        options={{headerShown: false}}
      />
    </RegisterStack.Navigator>
  );
};

const MainNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Home"
      component={HomeTabs}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="editAccount"
      component={EditAccount}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Bantuan"
      component={Bantuan}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Donasi"
      component={Donasi}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="DetailDonasi"
      component={DetailDonasi}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Create-Donation"
      component={CreateDonasi}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Payment"
      component={Payment}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="History"
      component={History}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Statistics"
      component={Statistics}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="RegisterStack"
      component={RegisterNavigation}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
};

export default AppNavigation;
