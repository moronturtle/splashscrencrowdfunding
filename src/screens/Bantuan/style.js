import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1},
  Header: {
    backgroundColor: 'blue',
    paddingLeft: 10,
    height: '9%',
    padding: 5,
  },
  textHeader: {
    padding: 10,
    marginBottom: 15,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  mapWrapper: {
    position: 'relative',
    height: '50%',
  },
  contentContainer: {},
  content: {
    margin: 20,
    flexDirection: 'row',
  },
  textContent: {
    marginLeft: 10,
  },
  hrLine: {
    marginTop: 4,
    width: '100%',
    backgroundColor: 'grey',
    opacity: 0.1,
    height: 2,
  },
});

export default styles;
