import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';

import styles from './style';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Icon from 'react-native-vector-icons/FontAwesome5';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoibW9yb250dXJ0bGUiLCJhIjoiY2trZHZydWJiMDU2cjJ4cDdvcWIwZ2w5aSJ9.DYJfjHCjo8O_tZmNw1ZXnQ',
);

const Bantuan = ({navigation}) => {
  const coordinates = [
    [107.58011, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766],
  ];

  let marker = coordinates.map((coordinate, index) => {
    return (
      <MapboxGL.PointAnnotation
        key={index}
        id={`pointAnnotation${index}`}
        coordinate={[coordinate[0], coordinate[1]]}>
        <MapboxGL.Callout
          title={`Longitude ${coordinate[0]} Latitude ${coordinate[1]}`}
        />
      </MapboxGL.PointAnnotation>
    );
  });

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={styles.textHeader}>Bantuan</Text>
      </View>
      <MapboxGL.MapView style={styles.mapWrapper}>
        <MapboxGL.Camera
          zoomLevel={5}
          centerCoordinate={[106.819449, -6.218465]}
        />
        {marker}
      </MapboxGL.MapView>
      <View style={styles.hrLine} />
      <View style={styles.contentContainer}>
        <View style={styles.content}>
          <Icon name="home" size={20} />
          <Text style={styles.textContent}>Jakarta, Bandung ,Yogyakarta</Text>
        </View>
        <View style={styles.hrLine} />
        <View style={styles.content}>
          <Icon name="at" size={20} />
          <Text style={styles.textContent}>
            customer_service@crowdfuncing.com
          </Text>
        </View>
        <View style={styles.hrLine} />
        <View style={styles.content}>
          <Icon name="phone" size={20} />
          <Text style={styles.textContent}>(021)777-888</Text>
        </View>
        <View style={styles.hrLine} />
      </View>
    </View>
  );
};
export default Bantuan;
