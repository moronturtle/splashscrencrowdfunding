import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import styles from './style';
import Icon from 'react-native-vector-icons/Feather';
import {SliderBox} from 'react-native-image-slider-box';
import colors from '../../style/colors';
//import {TextInput} from 'react-native-gesture-handler';

const Home = ({navigation}) => {
  const images = [
    require('../../assets/images/donate.jpeg'),
    require('../../assets/images/donate1.jpeg'),
    require('../../assets/images/donate2.jpeg'),
    require('../../assets/images/donate3.jpeg'),
    require('../../assets/images/donate4.jpeg'),
    require('../../assets/images/donate5.jpeg'),
  ];

  const data = [
    {
      id: 1,
      title: 'Lorem ipsum dolor sit amet.',
      total: 20000000,
      progress: 2000000,
      penggalang_dana: 'Park Ji Sung',
      images:
        'https://image.freepik.com/free-photo/people-holding-rubber-heart_1150-18576.jpg',
    },
    {
      id: 2,
      title: 'Lorem ipsum dolor sit amet.',
      total: 70000000,
      progress: 20000000,
      penggalang_dana: 'Jhon Doe',
      images:
        'https://image.freepik.com/free-photo/small-plant-with-coins_23-2147807265.jpg',
    },
    {
      id: 3,
      title: 'Lorem ipsum dolor sit amet.',
      total: 40000000,
      progress: 500000,
      penggalang_dana: 'KitaBisa.com',
      images:
        'https://image.freepik.com/free-vector/people-carrying-donation-charity-related-icons_53876-43091.jpg',
    },
    {
      id: 4,
      title: 'Lorem ipsum dolor sit amet.',
      total: 120000000,
      progress: 70000000,
      penggalang_dana: 'Sanbercode.com',
      images:
        'https://image.freepik.com/free-photo/hand-holding-red-heart-wood-background_1150-6891.jpg',
    },
    {
      id: 5,
      title: 'Lorem ipsum dolor sit amet.',
      total: 220000000,
      progress: 12000000,
      penggalang_dana: 'Kitabisa.com',
      images:
        'https://image.freepik.com/free-photo/beggars-sit-bridge-with-homeless-message-please-help_1150-22935.jpg',
    },
    {
      id: 6,
      title: 'Lorem ipsum dolor sit amet.',
      total: 320000000,
      progress: 62000000,
      penggalang_dana: 'Test',
      images:
        'https://image.freepik.com/free-photo/begging-bridge-with-person-who-handed-bread_1150-22948.jpg',
    },
  ];

  const renderItem = ({item}) => {
    var rupiah = '';
    var nilai = item.total.toString().split('').reverse().join('');
    for (var i = 0; i < nilai.length; i++)
      if (i % 3 == 0) rupiah += nilai.substr(i, 3) + '.';
    return (
      <View style={styles.itemList}>
        <Image source={{uri: item.images}} style={styles.imgList} />
        <View style={{padding: 10}}>
          <Text>{item.title}</Text>
          <Text style={styles.listText}>
            {'Rp. ' +
              rupiah
                .split('', rupiah.length - 1)
                .reverse()
                .join('')}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <ScrollView style={styles.scrollViewStyle}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.blue,
          }}>
          <Image
            source={require('../../assets/icons/flow.png')}
            style={[
              styles.iconMenu,
              {backgroundColor: colors.blue, marginLeft: 15},
            ]}
          />
          <Icon
            style={{padding: 5, backgroundColor: colors.blue}}
            name="search"
            size={20}
            color="#000"
          />
          <TextInput
            style={{
              flex: 1,
              paddingTop: 10,
              paddingRight: 10,
              paddingBottom: 10,
              paddingLeft: 10,
              backgroundColor: '#fff',
              color: '#424242',
              borderRadius: 20,
              marginRight: 5,
            }}
            placeholder="Search"
            underlineColorAndroid="transparent"
          />
          <Image
            source={require('../../assets/icons/donate_home.png')}
            style={[
              styles.iconMenu,
              {backgroundColor: colors.blue, marginRight: 15},
            ]}
          />
        </View>
        <View style={styles.header}>
          <View style={styles.cardContainer}>
            <View style={styles.imgContainer}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../assets/icons/purse.png')}
                  style={styles.purse}
                  resizeMode="contain"
                />
                <Text style={styles.textWhite}>Saldo</Text>
              </View>
              <Text style={styles.textWhite}>Rp. 0</Text>
            </View>
            <TouchableOpacity
              style={styles.iconContainer}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('TopUp')}>
              <Icon name="plus-square" size={25} />
              <Text>Isi</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} activeOpacity={0.7}>
              <Icon name="repeat" size={25} style={{marginBottom: 2}} />
              <Text>Riwayat</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} activeOpacity={0.7}>
              <Icon name="grid" size={25} style={{marginBottom: 2}} />
              <Text>Lainnya</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.cardContent}>
          <View>
            <SliderBox
              autoplay
              circleLoop
              images={images}
              sliderBoxHeight={200}
              dotColor={colors.blue}
              inactiveDotColor={colors.white}
              imageLoadingColor={colors.blue}
              ImageComponentStyle={{width: '92%', borderRadius: 5}}
            />
          </View>
          <View style={styles.menuContainer}>
            <TouchableOpacity
              style={styles.menuContent}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('Donasi')}>
              <Image
                source={require('../../assets/icons/donate.png')}
                style={styles.iconMenu}
              />
              <Text>Donasi</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuContent}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('Statistics')}>
              <Image
                source={require('../../assets/icons/trend.png')}
                style={styles.iconMenu}
              />
              <Text>Statistik</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuContent}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('History')}>
              <Image
                source={require('../../assets/icons/clock.png')}
                style={styles.iconMenu}
              />
              <Text>Riwayat</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuContent}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('Create-Donation')}>
              <Image
                source={require('../../assets/icons/charity.png')}
                style={styles.iconMenu}
              />
              <Text>Bantu</Text>
            </TouchableOpacity>
          </View>
          <View style={{height: 10, backgroundColor: '#e1e5eb'}}></View>
          <View style={styles.listContainer}>
            <Text style={styles.title}>Penggalangan Dana Mendesak</Text>
            <View style={{flex: 1}}>
              <FlatList
                data={data}
                horizontal
                renderItem={renderItem}
                key={(item) => item.id}
                style={{marginLeft: -20}}
                keyExtractor={(item) => item.id.toString()}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;
