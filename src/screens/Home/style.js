import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../style/colors';

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  scrollViewStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  header: {
    height: 70,
    backgroundColor: colors.blue,
  },
  cardContainer: {
    top: 20,
    left: 20,
    width: '90%',
    padding: 20,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  imgContainer: {
    padding: 7,
    borderRadius: 5,
    flexDirection: 'column',
    backgroundColor: colors.blue,
  },
  purse: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardContent: {
    flex: 1,
    marginTop: 60,
    flexDirection: 'column',
    backgroundColor: colors.white,
  },
  textWhite: {
    fontSize: 14,
    color: colors.white,
  },
  menuContainer: {
    padding: 15,
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  menuContent: {
    alignItems: 'center',
    flexDirection: 'column',
  },
  iconMenu: {
    width: 40,
    height: 40,
    marginBottom: 10,
  },
  listContainer: {
    height: 250,
    padding: 15,
    marginTop: -5,
    flexDirection: 'column',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  itemList: {
    flex: 1,
    top: 20,
    left: 20,
    width: '90%',
    height: 175,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginRight: 10,
    flexDirection: 'column',
    backgroundColor: colors.white,
  },
  imgList: {
    width: 250,
    height: 100,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  listText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  panel: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: colors.white,
  },
  panelContent: {
    padding: 20,
  },
  panelTitle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelTextTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default style;
