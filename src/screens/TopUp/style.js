import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../style/colors';

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.white,
  },
  inputContainer: {
    padding: 20,
    flexDirection: 'column',
  },
  input: {
    height: 50,
    color: colors.black,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGrey,
  },
  content: {
    padding: 20,
    paddingTop: -10,
    flexDirection: 'column',
  },
  btnMenu: {
    flex: 1,
    margin: 5,
    padding: 15,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  btnCheckout: {
    height: 50,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.blue,
  },
  btnTextCheckout: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.white,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default style;
