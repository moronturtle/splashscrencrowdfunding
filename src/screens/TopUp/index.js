import React, {useState, useRef, useEffect} from 'react';
import {View, Text, FlatList, ActivityIndicator} from 'react-native';
import {Button} from '../../components/Button';
import colors from '../../style/colors';
import styles from './style';
import Axios from 'axios';
import uri from '../../api';
import {TextInputMask} from 'react-native-masked-text';
import AsyncStorage from '@react-native-community/async-storage';

const topup_list = [
  {
    id: 'topup10k',
    total: 10000,
  },
  {
    id: 'topup20k',
    total: 20000,
  },
  {
    id: 'topup50k',
    total: 50000,
  },
  {
    id: 'topup100k',
    total: 100000,
  },
  {
    id: 'topup200k',
    total: 200000,
  },
  {
    id: 'topup500k',
    total: 500000,
  },
];

const TopUp = (props) => {
  let moneyField = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({});
  const [total, setTotal] = useState('');
  const [token, setToken] = useState(null);
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          setToken(token);
          return getProfile(token);
        }
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);

  const getProfile = (token) => {
    Axios.get(`${uri}/profile/get-profile`, {
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        const data = res.data.data.profile;
        setData(data);
      })
      .catch((err) => {
        console.log('Account -> err', err);
      });
  };

  const onSelect = (item) => {
    setSelected(item.total);
  };

  const onTopUpPress = () => {
    const time = new Date().getTime();
    const nominal = moneyField.current.getRawValue();
    const body = {
      transaction_details: {
        order_id: `Donasi-${time}`,
        gross_amount: selected !== null ? selected : nominal,
        donation_id: props.id,
      },
      customer_details: {
        first_name: data.name,
        email: data.email,
      },
    };

    setIsLoading(true);

    Axios.post(`${uri}/donasi/generate-midtrans`, body, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (res) {
          const data = res.data.data;
          setIsLoading(false);
          props.navigation.replace('Payment', data);
        }
      })
      .catch((err) => {
        console.log('err', err);
        alert(`Payment error - ${err}`);
        setIsLoading(false);
      });
  };

  const renderItem = ({item}) => {
    var rupiah = '';
    var nilai = item.total.toString().split('').reverse().join('');
    for (var i = 0; i < nilai.length; i++)
      if (i % 3 == 0) rupiah += nilai.substr(i, 3) + '.';
    const bgColor = item.total == selected ? colors.blue : colors.white;
    const fontColor = item.total == selected ? colors.white : colors.black;
    return (
      <View style={[styles.btnMenu, {backgroundColor: bgColor}]}>
        <Button onPress={() => onSelect(item)}>
          <Text style={{color: fontColor}}>
            {'Rp. ' +
              rupiah
                .split('', rupiah.length - 1)
                .reverse()
                .join('')}
          </Text>
        </Button>
      </View>
    );
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <Text>ISI NOMINAL</Text>
        <TextInputMask
          type={'money'}
          style={styles.input}
          options={{
            precision: 0,
            separator: ',',
            delimiter: '.',
            unit: 'RP. ',
            suffixUnit: '',
          }}
          value={total}
          ref={moneyField}
          placeholder="Rp. 0"
          onChangeText={(value) => setTotal(value)}
        />
      </View>
      <View style={styles.content}>
        <FlatList
          data={topup_list}
          renderItem={renderItem}
          numColumns={topup_list.length / 2}
        />
        <View style={{marginTop: 15}}>
          <Button style={styles.btnCheckout} onPress={() => onTopUpPress()}>
            <Text style={styles.btnTextCheckout}>LANJUT</Text>
          </Button>
        </View>
      </View>
    </View>
  );
};

export default TopUp;
