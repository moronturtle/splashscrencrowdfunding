import React from 'react'
import { StyleSheet } from 'react-native'
import colors from '../../style/colors'

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: colors.white
    }
})

export default style
