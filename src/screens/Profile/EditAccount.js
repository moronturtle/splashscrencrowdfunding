import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Modal,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../../style/colors';

import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import {RNCamera} from 'react-native-camera';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const EditAccount = ({navigation, route}) => {
  // klo menggunakan useRef perlu pake current sebelum methodnya
  let input = useRef(null);
  let camera = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [name, setName] = useState(route.params.name);
  const [email, setEmail] = useState(route.params.email);
  const [photo, setPhoto] = useState(null);
  const [editable, setEditable] = useState(false);
  const [token, setToken] = useState('');

  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  useEffect(() => {
    async function getToken() {
      try {
        const token = await Asyncstorage.getItem('token');
        setToken(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
  }, []);

  const editData = () => {
    setEditable(!editable);
  };

  const onSavePress = (data) => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('photo', {
      uri: data !== null ? data.uri : '',
      name: 'photo.jpg',
      type: 'image/jpg',
    });

    console.log('formData', formData);
    setIsLoading(true);
    Axios.post(`${api}/profile/update-profile`, formData, {
      timeout: 200000,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('res', res);
        Asyncstorage.setItem(`photo${email}`, data.uri);
        setIsLoading(false);
        alert('Success Updated Profile');
        navigation.push('Home');
      })
      .catch((err) => {
        setIsLoading(false);
        console.log('err', err);
        alert('Failed Updated Profile');
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}>
            <View>
              <TouchableOpacity
                style={styles.btnRotate}
                onPress={() => toggleCamera()}>
                <MaterialCommunity
                  name="rotate-3d-variant"
                  size={25}
                  color={'black'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View style={styles.oval} />
              <View style={styles.rectangle} />
              <View style={styles.captureContainer}>
                <TouchableOpacity
                  style={styles.btnCamera}
                  onPress={() => {
                    takePicture();
                  }}>
                  <Icon name="camera" size={40} color={'black'} />
                </TouchableOpacity>
              </View>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.Container}>
      {/* Header */}
      <View style={styles.Header}>
        <Text style={styles.textHeader}>My Profile</Text>
      </View>

      {/* Profile */}
      <View style={styles.profileName}>
        <Image
          style={styles.profileNameImage}
          source={{
            uri: photo != null ? photo.uri : route.params.photo,
          }}
        />
        <TouchableOpacity
          style={styles.openCamera}
          onPress={() => setIsVisible(true)}>
          <Icon name="camera" size={22} color={'white'} />
        </TouchableOpacity>
      </View>

      <Text style={styles.titleText}>Nama Lengkap</Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TextInput
          ref={input}
          value={name}
          editable={editable}
          style={styles.valueText}
          onChangeText={(value) => {
            setName(value);
          }}
        />
        <Icon
          name="edit"
          style={styles.editText}
          onPress={() => {
            editData();
          }}
        />
      </View>
      <Text style={styles.titleText}>Email</Text>
      <Text style={styles.valueText}>{email}</Text>

      <View style={{alignItems: 'center', marginVertical: 50}}>
        <TouchableOpacity
          style={styles.saveButton}
          onPress={() => {
            onSavePress(photo);
          }}>
          <Text style={{color: 'white', fontWeight: '700'}}>SAVE</Text>
        </TouchableOpacity>
      </View>
      {renderCamera()}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor: 'white',
  },
  Header: {
    flex: 1,
    backgroundColor: 'blue',
    padding: 25,
    paddingBottom: 45,
  },
  textHeader: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  profileName: {
    backgroundColor: 'white',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  titleText: {
    marginLeft: 20,
    marginBottom: 3,
    margin: 5,
    fontSize: 14,
    color: 'grey',
  },
  editText: {
    marginRight: 20,
    margin: 7,
    fontSize: 25,
    color: 'grey',
  },
  valueText: {
    marginLeft: 20,
    margin: 10,
    fontSize: 16,
    color: 'black',
  },
  profileNameImage: {
    height: 105,
    width: 105,
    borderRadius: 60,
  },
  btnCamera: {
    height: 80,
    width: 80,
    borderRadius: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnRotate: {
    margin: 10,
    height: 40,
    width: 40,
    borderRadius: 40,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rewind: {
    height: 80,
    width: 80,
    borderRadius: 60,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rectangle: {
    height: 120,
    width: 180,
    backgroundColor: 'transparent',
    borderColor: 'white',
    borderWidth: 2,
    marginTop: 30,
  },
  oval: {
    width: 100,
    height: 270,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: 'white',
    transform: [{scaleX: 2}],
    marginBottom: 30,
  },
  captureContainer: {
    //  alignSelf: 'center',
    bottom: 0,
    marginTop: 40,
    marginBottom: 20,
  },
  openCamera: {
    position: 'absolute',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: 40,
    borderRadius: 60,
    marginVertical: 60,
  },
  saveButton: {
    backgroundColor: 'blue',
    width: '90%',
    alignItems: 'center',
    padding: 20,
    borderRadius: 8,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default EditAccount;
