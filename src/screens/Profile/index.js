import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import colors from '../../style/colors';

import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

const Profile = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [profileName, setProfileName] = useState('');
  const [email, setEmail] = useState('');
  const [userInfo, setUserInfo] = useState(null);
  const [defaultPhoto, setDefaultPhoto] = useState(null);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await Asyncstorage.getItem('token');
        const profile = await getProfile(token);
        const photo = await Asyncstorage.getItem(`photo${email}`);
        setDefaultPhoto(
          photo != null
            ? photo
            : 'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png',
        );
        return profile;
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getCurrentUser();
  }, [email]);

  const getProfile = (token) => {
    setIsLoading(true);
    Axios.get(`${api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        setProfileName(res.data.data.profile.name);
        setEmail(res.data.data.profile.email);
        setIsLoading(false);
      })
      .catch((err) => {});
  };

  const getCurrentUser = async () => {
    const user = await GoogleSignin.signInSilently();
    setUserInfo(user);
  };
  const onLogOutPress = async () => {
    try {
      await Asyncstorage.removeItem('token');
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } catch (err) {
      console.log(err);
    }
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.Container}>
      {/* Header */}
      <View style={styles.Header}>
        <Text style={styles.textHeader}>Account</Text>
      </View>
      <ScrollView>
        {/* Profile */}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('editAccount', {
              name: profileName,
              email: email,
              photo: defaultPhoto,
            });
          }}>
          <View>
            <View style={styles.profileName}>
              <Image
                style={styles.profileNameImage}
                source={{
                  uri:
                    userInfo && userInfo.user
                      ? userInfo.user.photo
                      : defaultPhoto,
                }}
              />
              <Text style={styles.profileNameText}>
                {userInfo && userInfo.user ? userInfo.user.name : profileName}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.hrLine} />

        {/* Dana Atau Saldo */}
        <View style={styles.DanaContainer}>
          <Icon name="credit-card" size={30} color="grey" />
          <Text style={styles.itemText}>Saldo</Text>
          <Text style={styles.itemText2}>Rp. 120.000.000</Text>
        </View>

        <View style={styles.hrLine2} />

        {/* kolom container untuk kebawah */}
        <View style={{flexDirection: 'column'}}>
          <View style={styles.TextContainer}>
            <Icon name="cogs" size={30} color="grey" />
            <Text style={styles.itemText}>Pengaturan</Text>
          </View>

          <View style={styles.hrLine} />
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Bantuan');
            }}>
            <View style={styles.TextContainer}>
              <Icon name="question-circle" size={30} color="grey" />
              <Text style={styles.itemText}>Bantuan</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.hrLine} />

          <View style={styles.TextContainer}>
            <Icon name="fax" size={30} color="grey" />
            <Text style={styles.itemText}>Syarat Ketentuan</Text>
          </View>

          <View style={styles.hrLine2} />

          <TouchableOpacity
            onPress={() => {
              onLogOutPress();
            }}>
            <View style={styles.TextContainer}>
              <Icon name="door-open" size={30} color="grey" />
              <Text style={styles.itemText}>Keluar</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.hrLine} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {},
  Header: {
    flex: 1,
    backgroundColor: 'blue',
    padding: 25,
    paddingBottom: 45,
  },
  textHeader: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  profileName: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  profileNameText: {
    marginTop: 20,
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  profileNameImage: {
    height: 65,
    width: 65,
    borderRadius: 40,
  },
  hrLine: {
    backgroundColor: 'grey',
    opacity: 0.1,
    height: 2,
  },
  hrLine2: {
    backgroundColor: 'grey',
    opacity: 0.2,
    height: 5,
  },
  DanaContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  TextContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  itemText: {
    marginLeft: 20,
    marginVertical: 5,
    fontSize: 15,
  },
  itemText2: {
    marginLeft: 20,
    marginVertical: 5,
    fontSize: 15,
    marginLeft: 'auto',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Profile;
