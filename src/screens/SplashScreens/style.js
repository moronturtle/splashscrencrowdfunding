import { StyleSheet } from 'react-native'
import colors from '../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blue
    },
    quotesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    quotes: {
        fontSize: 14,
        color: colors.white
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    textLogo: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.white
    }
})

export default styles