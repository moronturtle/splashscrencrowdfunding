import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

const ListItem = ({item}) => {
  console.log(item);
  var rupiah = '';
  var nilai = item.item.amount.toString().split('').reverse().join('');
  for (var i = 0; i < nilai.length; i++)
    if (i % 3 == 0) rupiah += nilai.substr(i, 3) + '.';
  return (
    <View
      style={{
        marginTop: 20,
        marginLeft: 10,
        marginRight: 25,
      }}>
      <Text style={styles.price}>
        {`Total Rp. ${rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')}`}
      </Text>
      <Text>{`Donation Id: ${item.item.order_id}`}</Text>
      <Text>{`Tanggal Transaksi : ${item.item.updated_at}`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  price: {
    fontWeight: 'bold',
  },
  listItem: {
    margin: 20,
    padding: 15,
    backgroundColor: '#f8f8f8',
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
});

export default ListItem;
