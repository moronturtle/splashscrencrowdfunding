import React, {useEffect, useState} from 'react';
import {Text, View, FlatList, ActivityIndicator, Alert} from 'react-native';
import Axios from 'axios';
import uri from '../../api';
import Asyncstorage from '@react-native-community/async-storage';
import ListItem from './ListItem';
import colors from '../../style/colors';
import RNFetchBlob from 'rn-fetch-blob';

import styles from './styles';

const History = ({navigation}) => {
  const [history, setHistory] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getToken = async () => {
      setIsLoading(true);
      const token = await Asyncstorage.getItem('token');
      if (token != null) {
        getHistory(token);
      } else {
        setIsLoading(false);
        alert(
          `You can't Access this feature , please register to sanbercode to get Token`,
        );
        navigation.navigate('Home');
      }
    };

    getToken();
  }, []);

  const getHistory = (token) => {
    // Axios.get(`${uri}/donasi/riwayat-transaksi`, {
    //   timeout: 20000,
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    //   .then((res) => {
    //     setHistory(res.data.data.riwayat_transaksi);
    //   })
    //   .catch((err) => console.log(err));

    RNFetchBlob.config({
      trusty: true,
    })
      .fetch('GET', `${uri}/donasi/riwayat-transaksi`, {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      })
      .then((response) => {
        setIsLoading(false);
        const data = JSON.parse(response.data);
        // console.log('data', data);
        // console.log('getDonation -> data', data.data.riwayat_transaksi);
        setHistory(data.data.riwayat_transaksi);
      })
      .catch((err) => {
        setIsLoading(false);
        // console.log("Donasi -> err", err)
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={styles.textHeader}>History</Text>
      </View>
      <FlatList
        data={history}
        key={(item) => item.id}
        keyExtractor={(item) => item.id.toString()}
        renderItem={(item) => {
          return <ListItem item={item} />;
        }}
      />
    </View>
  );
};

export default History;
