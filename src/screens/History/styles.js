import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1},
  Header: {
    backgroundColor: 'blue',
    paddingLeft: 10,
    height: '12%',
    padding: 5,
  },
  textHeader: {
    padding: 15,
    marginBottom: 15,
    paddingTop: 25,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  scrollViewStyle: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
