import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import Axios from 'axios';
import uri from '../../api';
import database from '@react-native-firebase/database';
import {GiftedChat} from 'react-native-gifted-chat';
import Asyncstorage from '@react-native-community/async-storage';

import styles from './style';

const Inbox = ({navigation}) => {
  const [user, setUser] = useState({});
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await Asyncstorage.getItem('token');
        if (token != null) {
          return getProfile(token);
        }
      } catch (err) {
        console.log(error, err);
      }
    };

    getToken();
    getProfile();
    onRef();
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getProfile = (token) => {
    console.log('token', token);

    Axios.get(`${uri}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        const data = res.data.data.profile;
        console.log('data user', data);
        setUser(data);
        console.log('user', user);
      })
      .catch((err) => {
        console.log('error profile', err);
      });
  };

  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };

  const onSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={styles.textHeader}>Inbox</Text>
      </View>
      <GiftedChat
        messages={messages}
        onSend={(messages) => onSend(messages)}
        user={{
          _id: user.id,
          name: user.name,
          avatar: `https://crowdfunding.sanberdev.com${user.photo}`,
        }}
      />
    </View>
  );
};
export default Inbox;
