import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1},
  Header: {
    backgroundColor: 'blue',
    paddingLeft: 10,
    height: '12%',
    padding: 5,
  },
  textHeader: {
    padding: 15,
    marginBottom: 15,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
});

export default styles;
