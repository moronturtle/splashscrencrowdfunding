import React, {useState, useEffect} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import colors from '../../style/colors';

import Header from '../../components/Header';
import Input from '../../components/Input';
import FilledButton from '../../components/FilledButton';
import TextButton from '../../components/TextButton';
import ErrorText from '../../components/ErrorText';

import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchId from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  //menyimpan token ke asyncstorage

  const saveToken = async (token) => {
    try {
      await Asyncstorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '642732183614-18q474fsnb1gboq9arm40actjneioiva.apps.googleusercontent.com',
    });
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const signInWithGoogle = async () => {
    try {
      setIsLoading(false);
      const {idToken} = await GoogleSignin.signIn();

      const credential = auth.GoogleAuthProvider.credential(idToken);

      console.log(credential);

      auth().signInWithCredential(credential);
      setIsLoading(false);
      navigation.navigate('Home');
    } catch (e) {
      console.log('signin error', e);
    }
  };

  const signInwithFingerPrint = () => {
    setIsLoading(true);
    TouchId.authenticate('', config)
      .then((success) => {
        setIsLoading(false);
        navigation.reset({
          index: 0,
          routes: [{name: 'Home'}],
        });
      })
      .catch((error) => {
        setIsLoading(false);
        console.log('Authentiacation Failed');
      });
  };

  const onLoginPress = () => {
    let data = {
      email: email,
      password: password,
    };

    setIsLoading(true);
    Axios.post(`${api}/auth/login`, data, {
      timeout: 200,
    })
      .then((res) => {
        saveToken(res.data.data.token);
        setIsError(false);
        setIsLoading(false);
        navigation.reset({
          index: 0,
          routes: [{name: 'Home'}],
        });
      })
      .catch((err) => {
        setIsLoading(false);
        setIsError(true);
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Header style={styles.title}>Login</Header>
      <ErrorText error={isError ? `incorrect email or password` : ''} />
      <Input
        value={email}
        style={styles.input}
        placeholder="Email"
        keyboardType="email-address"
        onChangeText={(email) => setEmail(email)}
      />
      <Input
        value={password}
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        onChangeText={(password) => setPassword(password)}
      />
      <FilledButton
        title="Login"
        style={styles.loginButton}
        onPress={() => onLoginPress()}
      />
      <GoogleSigninButton
        onPress={() => {
          signInWithGoogle();
        }}
        style={{width: '100%', height: 40}}
      />
      <TouchableOpacity
        style={{
          width: '98%',
          height: 5,
          backgroundColor: '#191970',
          alignItems: 'center',
          padding: 5,
          paddingBottom: 30,
          borderRadius: 3,
          marginBottom: 10,
        }}
        onPress={() => {
          signInwithFingerPrint();
        }}>
        <Text style={{color: 'white'}}>Sign In With Fingerprint</Text>
      </TouchableOpacity>

      <TextButton
        title="Have u an account ? create one"
        onPress={() => {
          navigation.navigate('RegisterStack');
        }}
      />
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 80,
    alignItems: 'center',
  },
  title: {
    marginBottom: 48,
  },
  input: {
    marginVertical: 8,
  },
  loginButton: {
    marginVertical: 32,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
