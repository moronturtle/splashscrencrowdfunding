import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1},
  Header: {
    backgroundColor: 'blue',
    paddingLeft: 10,
    height: '12%',
    padding: 5,
    marginBottom: 10,
  },
  textHeader: {
    padding: 15,
    marginBottom: 0,
    paddingTop: 25,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  Cameracontainer: {
    backgroundColor: '#E0E0E0',
    width: '100%',
    height: '26%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  PictureContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageDonate: {
    marginTop: 20,
    width: '100%',
    height: '80%',
  },
  btnRotate: {
    margin: 10,
    height: 40,
    width: 40,
    borderRadius: 40,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnCamera: {
    height: 80,
    width: 80,
    borderRadius: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
