import React, {useEffect, useState, useRef} from 'react';
import {
  Alert,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Modal,
  Image,
  ActivityIndicator,
} from 'react-native';
import colors from '../../style/colors';
import Axios from 'axios';
import uri from '../../api';
import Asyncstorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TextInputMask} from 'react-native-masked-text';
import {RNCamera} from 'react-native-camera';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';

const CreateDonasi = ({navigation}) => {
  let camera = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [judul, setJudul] = useState('');
  const [dana, setDana] = useState('0');
  const [deskripsi, setDeskripsi] = useState('');
  const [photo, setPhoto] = useState(null);
  const [token, setToken] = useState('');

  useEffect(() => {
    const getToken = async () => {
      const token = await Asyncstorage.getItem('token');
      setToken(token);
    };

    getToken();
  }, []);

  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const onSaveDana = () => {
    const formData = new FormData();
    formData.append('title', judul);
    formData.append('description', deskripsi);
    formData.append('donation', dana.replace('.', ''));
    formData.append('photo', {
      uri: photo !== null ? photo.uri : '',
      name: 'photo.jpg',
      type: 'image/jpg',
    });

    setIsLoading(true);
    Axios.post(`${uri}/donasi/tambah-donasi`, formData, {
      timeout: 20000,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        setIsLoading(false);
        Alert.alert('Success Create Donation');
        navigation.push('Home');
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        const messages =
          token !== null
            ? 'Failed Create Donation'
            : `You can't create donation , please register to sanbercode to get Token`;
        Alert.alert(messages);
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}>
            <View>
              <TouchableOpacity
                style={styles.btnRotate}
                onPress={() => toggleCamera()}>
                <MaterialCommunity
                  name="rotate-3d-variant"
                  size={25}
                  color={'black'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View />
              <View />
              <View>
                <TouchableOpacity
                  style={styles.btnCamera}
                  onPress={() => {
                    takePicture();
                  }}>
                  <Icon name="camera" size={40} color={'black'} />
                </TouchableOpacity>
              </View>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <Text style={styles.textHeader}>Create Donasi</Text>
      </View>

      <View style={styles.Cameracontainer}>
        <Image
          source={{
            uri: photo != null ? photo.uri : 'empty',
          }}
          style={photo != null ? styles.imageDonate : ''}
        />
        <TouchableOpacity onPress={() => setIsVisible(true)}>
          <Icon name="camera" size={35} color={'grey'} />
        </TouchableOpacity>
        <Text>Pilih Gambar</Text>
      </View>
      <View style={{margin: 20}}>
        <Text>Judul</Text>
        <TextInput
          placeholder={`Judul`}
          value={judul}
          style={{borderBottomWidth: 1, borderBottomColor: '#E0E0E0'}}
          onChangeText={(value) => {
            setJudul(value);
          }}
        />
      </View>
      <View style={{margin: 20}}>
        <Text>Deskripsi</Text>
        <TextInput
          placeholder={`Deskripsi`}
          value={deskripsi}
          style={{borderBottomWidth: 1, borderBottomColor: '#E0E0E0'}}
          onChangeText={(value) => {
            setDeskripsi(value);
          }}
        />
      </View>
      <View style={{margin: 20}}>
        <Text>Dana Yang Dibutuhkan</Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={{marginTop: '4%'}}>Rp.</Text>
          <TextInputMask
            type={'cpf'}
            value={dana}
            onChangeText={(text) => {
              setDana(text);
            }}
          />
        </View>
      </View>
      <View style={{alignItems: 'center'}}>
        <TouchableOpacity
          style={{
            width: '82%',
            height: '7%',
            padding: 20,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'blue',
          }}
          onPress={() => {
            onSaveDana();
          }}>
          <Text style={{color: 'white'}}>BUAT</Text>
        </TouchableOpacity>
      </View>
      {renderCamera()}
    </View>
  );
};

export default CreateDonasi;
