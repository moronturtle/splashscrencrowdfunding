import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Axios from 'axios';

import Header from '../../components/Header';
import TextButton from '../../components/TextButton';

import api from '../../api';
import colors from '../../style/colors';

const Otp = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState(route.params.email);
  const [pin, setPin] = useState('');

  const onVerificationPress = () => {
    let data = {
      otp: pin,
    };

    setIsLoading(true);
    Axios.post(`${api}/auth/verification`, data, {
      timeout: 200,
    })
      .then((res) => {
        //console.log('result otp', res);
        setIsLoading(false);
        navigation.navigate('RegisterPassword', {email: email});
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        alert('Verification code is failed , retry');
      });
  };

  const onRegenaratePress = () => {
    let data = {
      email: email,
    };

    setIsLoading(true);
    Axios.post(`${api}/auth/regenerate-otp`, data, {
      timeout: 200,
    })
      .then((res) => {
        setIsLoading(false);
        alert('Regenerate OTP successfull, please check your email');
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        alert('Resend Email Failed');
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Header style={styles.title}>Register</Header>
      <View style={{flexDirection: 'row'}}>
        <Text>
          Masuka 6 digit kode yang dikirimkan ke{' '}
          <Text style={{color: 'blue'}}>{email}</Text>
        </Text>
      </View>

      <OTPInputView
        style={{width: '80%', height: 200, color: 'black'}}
        pinCount={6}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={(code) => {
          console.log(`Code is ${code}, you are good to go!`);
          setPin(code);
        }}
      />
      <TouchableOpacity
        style={styles.verButton}
        onPress={() => {
          onVerificationPress();
        }}>
        <Text style={{color: 'white', fontWeight: '700'}}>Verifikasi</Text>
      </TouchableOpacity>
      <Text>Belum menerima kode verivikasi</Text>
      <TextButton
        title="kirim ulang"
        style={{color: 'blue'}}
        onPress={() => {
          onRegenaratePress();
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 90,
    alignItems: 'center',
  },
  title: {
    marginBottom: 20,
    paddingBottom: 5,
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: 'black',
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 4,
    borderBottomColor: 'black',
    color: 'black',
  },

  underlineStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  verButton: {
    backgroundColor: 'blue',
    width: '90%',
    alignItems: 'center',
    padding: 20,
    borderRadius: 8,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Otp;
