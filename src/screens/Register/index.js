import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View, ActivityIndicator} from 'react-native';
import Axios from 'axios';

import Header from '../../components/Header';
import Input from '../../components/Input';
import FilledButton from '../../components/FilledButton';
import ErrorText from '../../components/ErrorText';
import IconButton from '../../components/IconButton';
import TextButton from '../../components/TextButton';
import colors from '../../style/colors';

import api from '../../api';

const RegistrationScreen = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');

  const onRegisterPress = () => {
    let data = {
      name: name,
      email: email,
    };

    setIsLoading(true);
    Axios.post(`${api}/auth/register`, data, {
      timeout: 200,
    })
      .then((res) => {
        setEmail(res.data.data.user.email);
        setIsLoading(false);
        navigation.navigate('Otp', {email: email});
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        alert('Email has registered , try another email');
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Header style={styles.title}>Registration</Header>
      <IconButton
        style={styles.closeIcon}
        name="close-circle-outline"
        onPress={() => {
          navigation.pop();
        }}
      />
      <ErrorText error={''} />
      <Input
        value={email}
        style={styles.input}
        placeholder="Email"
        keyboardType="email-address"
        onChangeText={(email) => setEmail(email)}
      />
      <Input
        value={name}
        style={styles.input}
        placeholder="Name"
        onChangeText={(name) => setName(name)}
      />
      <FilledButton
        title="Register"
        style={styles.registerButton}
        onPress={() => onRegisterPress()}
      />

      <TextButton
        title="Already have a account ? Login"
        onPress={() => {
          navigation.navigate('Login');
        }}
      />
    </View>
  );
};

export default RegistrationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 90,
    alignItems: 'center',
  },
  title: {
    marginBottom: 48,
  },
  input: {
    marginVertical: 8,
  },
  registerButton: {
    marginVertical: 32,
  },
  closeIcon: {
    position: 'absolute',
    top: 60,
    right: 16,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
