import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Axios from 'axios';

import Header from '../../components/Header';
import Input from '../../components/Input';

import api from '../../api';
import colors from '../../style/colors';

const InputPassword = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState(route.params.email);
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');

  const onSavePress = () => {
    let data = {
      email: email,
      password: password,
      password_confirmation: rePassword,
    };

    //console.log('data', data);

    setIsLoading(true);
    Axios.post(`${api}/auth/update-password`, data, {
      timeout: 200,
    })
      .then((res) => {
        setIsLoading(false);
        navigation.navigate('Login');
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
        alert('Password and Re-Enter Password not matched');
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Header style={styles.title}>Input Your Password</Header>
      <Text style={{marginBottom: 40}}>
        Tentukan kata sandi untuk keamanan akun anda
      </Text>
      <Input
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={(password) => setPassword(password)}
      />
      <Input
        style={styles.input}
        placeholder="Re-Enter Password"
        secureTextEntry
        value={rePassword}
        onChangeText={(rePassword) => setRePassword(rePassword)}
      />
      <TouchableOpacity
        style={styles.saveButton}
        onPress={() => {
          onSavePress();
        }}>
        <Text style={{color: 'white', fontWeight: '700'}}>Save</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 90,
    alignItems: 'center',
  },
  title: {
    marginBottom: 20,
    paddingBottom: 5,
  },
  saveButton: {
    backgroundColor: 'blue',
    width: '90%',
    alignItems: 'center',
    padding: 20,
    borderRadius: 8,
  },
  input: {
    marginVertical: 8,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default InputPassword;
