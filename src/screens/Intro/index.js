import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import styles from './style';
import colors from '../../style/colors';
import {Button} from '../../components/Button';
import AppIntroSlider from 'react-native-app-intro-slider';

import Asyncstorage from '@react-native-community/async-storage';
import {get} from 'react-native/Libraries/TurboModule/TurboModuleRegistry';

const data = [
  {
    id: 1,
    image: require('../../assets/images/Online-Education.jpeg'),
    description: 'Test Project React Native Giri',
  },
  {
    id: 2,
    image: require('../../assets/images/Online-Education1.jpeg'),
    description: 'Description Text 2',
  },
  {
    id: 3,
    image: require('../../assets/images/Online-Education2.jpeg'),
    description: 'Description Text 3',
  },
];

const Intro = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getAsynStorage = async () => {
      try {
        setIsLoading(true);
        const getToken = await Asyncstorage.getItem('token');
        if (getToken != null) {
          setIsLoading(false);
          navigation.navigate('Home');
        }
        setIsLoading(false);
      } catch (err) {
        console.log(err);
      }
    };

    getAsynStorage();
  }, []);

  const renderItem = ({item}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image
            source={item.image}
            style={styles.imgList}
            resizeMethod="auto"
            resizeMode="contain"
          />
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    );
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
        <View style={styles.textLogoContainer}>
          <Text style={styles.textLogo}>CrowdFunding</Text>
        </View>
        <View style={styles.slider}>
          <AppIntroSlider
            data={data}
            renderItem={renderItem}
            renderNextButton={() => null}
            renderDoneButton={() => null}
            activeDotStyle={styles.activeDotStyle}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
        <View style={styles.btnContainer}>
          <Button
            style={styles.btnLogin}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnTextLogin}>MASUK</Text>
          </Button>
          <Button
            style={styles.btnRegister}
            onPress={() => navigation.navigate('RegisterStack')}>
            <Text style={styles.btnTextRegister}>DAFTAR</Text>
          </Button>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Intro;
