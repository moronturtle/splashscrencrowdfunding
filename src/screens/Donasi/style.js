import React from 'react'
import { StyleSheet, Dimensions } from 'react-native'
import colors from '../../style/colors'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listContainer: {
        marginTop: 5,
        flexDirection: 'column',
        backgroundColor: colors.white
    },
    listContent: {
        margin: 10,
        flexDirection: 'row',
    },
    images: {
        width: 200,
        height: 130,
        borderRadius: 5
    },
    description: {
        flex: 1,
        marginLeft: 10
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    donationDescription: {
        fontSize: 14,
        marginTop: 10,
        fontWeight: 'bold'
    },
    subTitle: {
        fontSize: 12,
        marginTop: 5,
        marginBottom: 7
    },
    price: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    imgBackground: {
        width: width,
        height: 250
    },
    content: {
        flex: 1,
        flexDirection: 'column'
    },
    detailDescription: {
        padding: 20,
        backgroundColor: colors. white
    },
    infoDescription: {
        flex: 1,
        padding: 20,
        marginTop: 5,
        backgroundColor: colors.white
    },
    cardContainer: {
        padding: 20,
        marginTop: 10,
        width: '100%',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexDirection: 'column',
        backgroundColor: colors.white
    },
    btnCheckout: {
        height: 50,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.blue
    },
    btnTextCheckout: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    profile: {
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    roundedImage: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: colors.transparent
    },
    modalContent: {
        height: 350,
        paddingTop: 12,
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
        backgroundColor: colors.white
    }
})

export default style
