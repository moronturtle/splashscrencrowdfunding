import React, {useState} from 'react';
import {View, Text, Modal, Image, ScrollView, StatusBar} from 'react-native';
import {Button} from '../../components/Button';
import styles from './style';
import uri from '../../api';
import colors from '../../style/colors';
import TopUp from '../TopUp';

const Detail = ({navigation, route}) => {
  const [isVisible, setIsVisible] = useState(false);

  var rupiah = '';
  let images = `https://crowdfunding.sanberdev.com${route.params.photo}`;
  var nilai = route.params.donation.toString().split('').reverse().join('');
  for (var i = 0; i < nilai.length; i++)
    if (i % 3 == 0) rupiah += nilai.substr(i, 3) + '.';

  const renderDonation = () => {
    return (
      <Modal
        animationType="fade"
        transparent
        visible={isVisible}
        onRequestClose={() => setIsVisible(false)}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TopUp navigation={navigation} id={route.params.id} />
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
        <View>
          <Image source={{uri: images}} style={styles.imgBackground} />
        </View>
        <View style={styles.content}>
          <View style={styles.detailDescription}>
            <Text style={[styles.title, {marginBottom: 10}]}>
              {route.params.title}
            </Text>
            <Text style={{marginTop: -5}}>
              Dana yang dibutuhkan{' '}
              {'Rp. ' +
                rupiah
                  .split('', rupiah.length - 1)
                  .reverse()
                  .join('')}
            </Text>
            <Text style={styles.donationDescription}>Deskripsi</Text>
            <Text style={styles.subTitle}>{route.params.description}</Text>
            <View style={{marginTop: 15}}>
              <Button
                style={styles.btnCheckout}
                onPress={() => setIsVisible(true)}>
                <Text style={styles.btnTextCheckout}>DONASI SEKARANG</Text>
              </Button>
            </View>
          </View>
          <View style={styles.infoDescription}>
            <Text style={styles.title}>Informasi Penggalangan Dana</Text>
            <View style={styles.cardContainer}>
              <Text>Penggalang Dana</Text>
              <View style={styles.profile}>
                <Image
                  source={
                    route.params.user.photo === null
                      ? require('../../assets/images/profile_picture.png')
                      : {
                          uri: `https://crowdfunding.sanberdev.com${route.params.user.photo}`,
                        }
                  }
                  style={styles.roundedImage}
                />
                <View style={{marginLeft: 10}}>
                  <Text style={styles.title}>{route.params.user.name}</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        {renderDonation()}
      </View>
    </ScrollView>
  );
};

export default Detail;
