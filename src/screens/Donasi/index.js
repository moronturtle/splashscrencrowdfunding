import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {ProgressBar} from 'react-native-paper';
import styles from './style';
import api from '../../api';
import Axios from 'axios';
import colors from '../../style/colors';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';

const Donasi = ({navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getToken = async () => {
      try {
        setIsLoading(true);
        const value = await AsyncStorage.getItem('token');
        if (value !== null) {
          getDonation(value);
        } else {
          setIsLoading(false);
          alert(
            `You can't Access this feature , please register to sanbercode to get Token`,
          );
          navigation.navigate('Home');
        }
      } catch (e) {
        console.log(e);
      }
    };
    getToken();
  }, []);

  const getDonation = (token) => {
    console.log('token donasi', token);
    RNFetchBlob.config({
      trusty: true,
    })
      .fetch('GET', `${api}/donasi/daftar-donasi`, {
        Authorization: 'Bearer' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      })
      .then((response) => {
        setIsLoading(false);
        const data = JSON.parse(response.data);
        console.log('getDonation -> data', data);
        setData(data.data.donasi);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log('Donasi -> err', err);
      });
  };

  const renderItem = ({item}) => {
    var rupiah = '';
    let images = `https://crowdfunding.sanberdev.com${item.photo}`;
    var nilai = item.donation.toString().split('').reverse().join('');
    for (var i = 0; i < nilai.length; i++)
      if (i % 3 == 0) rupiah += nilai.substr(i, 3) + '.';

    return (
      <TouchableOpacity
        style={styles.listContainer}
        activeOpacity={0.7}
        onPress={() => navigation.navigate('DetailDonasi', item)}>
        <View style={styles.listContent}>
          <Image
            source={{uri: images}}
            style={styles.images}
            resizeMethod="resize"
          />
          <View style={styles.description}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.subTitle}>
              {item.user === null ? '' : item.user.name}
            </Text>
            <ProgressBar progress={0.2} style={{marginBottom: 5}} />
            <Text style={styles.subTitle}>Dana yang dibutuhkan</Text>
            <Text style={styles.price}>
              {'Rp. ' +
                rupiah
                  .split('', rupiah.length - 1)
                  .reverse()
                  .join('')}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  if (isLoading) {
    return (
      <ActivityIndicator
        size="large"
        color={colors.blue}
        style={styles.loading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
      <FlatList
        data={data}
        extraData={data}
        renderItem={renderItem}
        key={(item) => item.id}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

export default Donasi;
